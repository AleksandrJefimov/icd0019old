package concurrent;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Optional;
import java.util.stream.IntStream;

public class HashCracker {

    private static final String ALPHABET = getAlphabet();
    private static final String HASH_TO_CRACK_1 =
            "d4faeb0660ce6d23883eced24805d2ff0da3600685e2ac9ad1dedd8cc66104ef";
    private static final String HASH_TO_CRACK_2 =
            "5443bf770be3146692aaeea227235917bc36cf1cbcf366f239f160a3ef149fee";

    public static void main(String[] args) {
        // String plainText = "hello";

        // System.out.println(sha256("2cf24dba5fb0a30e26e83b2ac5b9e29e1b161e5c1fa7425e73043362938b9824"));

        // String hashToCrack = "5443bf770be3146692aaeea227235917bc36cf1cbcf366f239f160a3ef149fee";

        // System.out.println(sha256("h7"));

        // System.out.println(ALPHABET);

        //for (char c1 : ALPHABET.toCharArray()) {
        //    for (char c2 : ALPHABET.toCharArray()) {
        //        for (char c3 : ALPHABET.toCharArray()) {
        //            for (char c4 : ALPHABET.toCharArray()) {
        //                String combination = String.valueOf(c1) + c2 + c3 + c4;
        //                String hash = sha256(combination);
        //                if (hash.equals(HASH_TO_CRACK_2)) {
        //                    System.out.println(combination);
        //                }
        //            }
        //        }
        //    }
        //}

        //System.out.println(decToString(10000));
        //for (int i = 0; i < 10000000; i++) {
        //    System.out.println(decToString(i));
        //}

        //int total = 30_000_000;
        //int partCount = 32;
        //int chuckSize = total / partCount;
        //int start = 0;
        // ExecutorService pool = Executors.newFixedThreadPool(8);
        //while (start < total) {
        //    int end = start + chuckSize;
        //    int startPos = start;
        //    pool.submit(() -> checkRange(startPos, end, HASH_TO_CRACK_2));
        //    start = end;
        //}
        //waitTillAllDone(pool);

        //Optional<String> first = IntStream.range(0, 30_000_000)
        //        .parallel()
        //        .mapToObj(i -> decToString(i))
        //        .filter(combination -> sha256(combination).equals(HASH_TO_CRACK_2 ))
        //        .findFirst();
        //System.out.println(first);
        //System.out.println("\n" + timer.getPassedTime());
        String first = "1";
        String second = "2";
        Timer timer = new Timer();
        Optional<String> count = IntStream.range(0, 30_000_000)
                .parallel()
                .mapToObj(HashCracker::decToString)
                .filter(combination -> sha256(combination).equals(HASH_TO_CRACK_2))
                .findFirst();
        System.out.println(count);
        System.out.println(first);
        System.out.println(second);
        System.out.println(timer.getPassedTime());
    }

    private static void checkRange(int start, int end, String hashToMatch) {
        for (int i = start; i < end; i++) {
            String hash = sha256(decToString(i));
            if (hash.equals(hashToMatch)) {
                System.out.println(decToString(i));
                return;
            }
        }
    }

    private static String decToString(int decimal) {
        if (decimal == 0) {
            return String.valueOf(ALPHABET.charAt(0));
        }
        StringBuilder builder = new StringBuilder();
        while (decimal > 0) {
            int reminder = decimal % ALPHABET.length();
            builder.append(ALPHABET.charAt(reminder));
            decimal /= ALPHABET.length();
        }
        return builder.reverse().toString();
    }

    private static String sha256(String plainText) {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }

        byte[] bytes = digest.digest(plainText.getBytes(StandardCharsets.UTF_8));

        return bytesToHex(bytes);
    }

    private static String bytesToHex(byte[] bytes) {
        StringBuilder hexString = new StringBuilder();
        for (byte byteValue : bytes) {
            String hex = Integer.toHexString(0xff & byteValue);
            if(hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }

        return hexString.toString();
    }

    private static String getAlphabet() {
        StringBuilder sb = new StringBuilder();
        for (char i = 48; i < 123; i++) {
            sb.append(i);
        }

        return sb.toString();
    }
}

