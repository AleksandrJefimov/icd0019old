package generics.cart;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ShoppingCart<T extends CartItem> {

    List<T> list = new ArrayList<T>();
    int quantity = 1;

    public String toString(){
        // String ret = "";
        // String retF;
        // quantity = 1;
        // for (T elem : list) {
        //     ret += (elem.getId());
        //     // ret.toString().add(elem.getPrice());
        //     // ret.toString().add(quantity);
        // }
        // // System.out.println(list.get(0).getId());
        // // String ret = String.valueOf(list.get(0));
        // retF = ret.toString();
        // return retF;
        System.out.println(list.get(0).toString());
        String al = "";
        int i = 0;
        for (T elem : list) {
            al += (helpToString(elem));
            if (i == 0){
                i++;
                al += ", ";
            }
        }
        return String.valueOf(al);
    }

    public String helpToString(T list) {

        String ret = "(" + list.getId() + ", " + list.getPrice()+ ", "  + quantity + ")";
        return ret;
    }

    public void add(Object item) {
        list.add((T) item);
    }

    public void removeById(String id) {
        int count = 0;
        int withId = 0;
        for (T elem : list) {
            if (elem.getId().equals(id)) {
                withId = count;
            }
            count += 1;
        }
        list.remove(withId);
    }

    public Double getTotal() {
        Double total = 0.0;
        for (T elem : list) {
            total += elem.getPrice();
        }
        return total;
    }

    public void increaseQuantity(String id) {

    }

    public void applyDiscountPercentage(Double discount) {
        throw new RuntimeException("not implemented yet");
    }

    public void removeLastDiscount() {
        throw new RuntimeException("not implemented yet");
    }

    public void addAll(Object items) {
        List<T> itemS = new ArrayList<T>((Collection<? extends T>) items);
        for (T item1: itemS) {
            list.add((T) item1);
        }
    }
}
