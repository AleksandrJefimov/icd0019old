package collections.cache;

import oo.hide.Timer;
import org.junit.Test;

public class Runner {

    @Test
    public void calculatesFibonacciOfN() {
        Fibonacci fib = new Fibonacci();

        Timer timer = new Timer();

        // System.out.println(fib.fib(1));
        // System.out.println(fib.fib(2));
        // System.out.println(fib.fib(3));
        // System.out.println(fib.fib(4));
        // System.out.println(fib.fib(5));
        // System.out.println(fib.fib(6));
        // System.out.println(fib.fib(7));

        for (int i = 0; i < 41; i++) {
            System.out.println(fib.fib(i));
        }


        fib.fib(40);

        System.out.println(timer.getPassedTime());
    }

}
