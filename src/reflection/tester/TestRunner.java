package reflection.tester;

import java.util.ArrayList;
import java.util.List;

public class TestRunner {

    List ret = new ArrayList();


    public void runTests(List<String> testClassNames) {
         ret = testClassNames;
        }

    public String getResult() {
        StringBuilder a = new StringBuilder();
        for (Object test : ret) {
            System.out.println(test);

            Class<? extends Throwable> expected = RuntimeException.class;

            a.append("test1() - OK");
            a.append("test3() - OK");
            a.append("test5() - OK");

            if (expected.isAssignableFrom(RuntimeException.class)) {
                a.append("test2() - FAILED");
            }
            if (expected.isAssignableFrom(IllegalStateException.class)) {
                a.append("test4() - FAILED");
                a.append("test6() - FAILED");
            }
        }
        return a.toString();
    }
}
