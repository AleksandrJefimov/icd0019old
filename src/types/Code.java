package types;

public class Code {

    public static void main(String[] args) {

        // int[] numbers = {1, 3, -2, 9};
        // System.out.println(sum(numbers)); // 11
        // System.out.println(average(numbers)); // 2.75
        // System.out.println(minimumElement(numbers)); // -2
        // System.out.println(asString(numbers)); // 1, 3, -2, 9
        // System.out.println(squareDigits("a1b2c4")); // a1b4c16
    }

    public static int sum(int[] numbers) {
        Integer sum = 0;
        for (Integer number : numbers){
            sum += number;
        }
        return sum;
    }

    public static double average(int[] numbers) {
        Double sum = 0.0;
        for (Integer number : numbers){
            sum += number;
        }
        return sum / numbers.length;
    }

    public static Integer minimumElement(int[] integers) {
        if (integers.length == 0) {
            return null;
        }

        Integer min = integers[0];

        for (Integer each : integers) {
            if (each < min) {
                min = each;
            }
        }

        return min;
    }

    public static String asString(int[] elements) {
        StringBuilder result = new StringBuilder();

        for (Integer element : elements) {
            if (element == elements[elements.length - 1]){
                result.append(element);
                return result.toString();
            }
            result.append(element).append(", ");
        }
        return "";
    }

    public static String squareDigits(String s) {
        StringBuilder result = new StringBuilder();
        char[] a = s.toCharArray();
        for (Character elem: a) {
            if (Character.isDigit(elem)){
                String x = Character.toString(elem);
                int y = Integer.parseInt(x);
                int num = y * y;
                result.append(num);
            }
            if (Character.isAlphabetic(elem)) {
                result.append(elem);
            }
        }
        return result.toString();
    }

}
