package junit;


public class Code {

    public static boolean isSpecial(int number) {

        return number % 11 < 4;
    }

    public static int longestStreak(String input) {
        if (input == null) {
            return 0;
        }

        int longest = 0;
        Character lastChar = null;
        int currentStreakLength = 0;

        for (Character currentChar : input.toCharArray()) {
            if (currentChar.equals(lastChar)) {
                currentStreakLength++;
            }
            else {
                currentStreakLength = 1;
            }

            if (currentStreakLength > longest) {
                longest = currentStreakLength;
            }

            lastChar = currentChar;
        }

        return longest;
    }

    public static Character mode(String input) {
        if (input == null) {
            return null;
        }

        int modeCount = 0;
        Character mode = null;

        for (char c : input.toCharArray()) {
            int count = getCharacterCount(input, c);

            if (count > modeCount) {
                modeCount = count;
                mode = c;
            }

        }
        return mode;
    }

    public static int getCharacterCount(String input, char c) {
        int count = 0;
        for (char c1 : input.toCharArray()) {
            if (c1 == c) {
                count = count + 1;
            }
        }
        return count;
    }

    public static int[] removeDuplicates(int[] input) {
        int[] withoutDuplicates = new int[input.length];
        int i = 0, j =0, retLength = 0, isZero = 0;
        for (int number1 : input) {
            for (int number2 : withoutDuplicates) {
                if (number1 != number2 && number1 != 0) {
                    j++;
                }
                if (number1 == 0 && isZero == 0) {
                    withoutDuplicates[i] = number1;
                    i++;
                    retLength++;
                    isZero = 1;
                }
                if (j >= input.length ){
                    withoutDuplicates[i] = number1;
                    i++;
                    retLength++;
                }
            }
            j = 0;
        }
        int[] ret = new int[retLength];
        int b = 0;
        while (b < (retLength)) {
            ret[b] = withoutDuplicates[b];
            b++;
        }
        return ret;
    }

    public static int sumIgnoringDuplicates(int[] integers) {
        int[] withoutDuplicates = removeDuplicates(integers);
        int sum = 0;
        int i = 0;
        while (i < withoutDuplicates.length) {
            sum += withoutDuplicates[i];
            i++;
        }
        return sum;
    }
}
