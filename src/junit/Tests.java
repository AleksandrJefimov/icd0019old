package junit;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.*;

public class Tests {

    @Test
    public void equalityExamples() {
        assertEquals(1, 1);
        assertThat(1, is (not(2)));

        int x2 = 1;
        Integer y2 = 1;
        assertThat(x2, is(equalTo(y2)));

        Integer x = 128;
        Integer y = 128;
        assertThat(x, is (y));
        assertEquals(y, x);

        assertSame("abc", "abc");
        assertSame("abc", "a" + "bc");

        String a = "a";
        assertNotSame("abc", a + "bc");
        assertEquals("abc", a + "bc");

    }

    @Test
    public void assertThatAndAssertEqualsExample() {

    }

    @Test
    public void findsSpecialNumbers() {
        assertTrue(Code.isSpecial(0)); // T
        assertTrue(Code.isSpecial(1)); // T
        assertTrue(Code.isSpecial(2)); // T
        assertTrue(Code.isSpecial(3)); // T
        assertFalse(Code.isSpecial(4)); // F
        assertTrue(Code.isSpecial(11)); // T
        assertFalse(Code.isSpecial(15)); // F
        assertTrue(Code.isSpecial(36)); // T
        assertFalse(Code.isSpecial(37));// F
        // other test cases for isSpecial() method
    }

    @Test
    public void findsLongestStreak() {
        assertThat(Code.longestStreak(""), is(0));
        assertThat(Code.longestStreak(null), is(0));
        assertThat(Code.longestStreak("abbcccaaaad"), is(4));
        assertThat(Code.longestStreak("a"), is(1));


        // other test cases for longestStreak() method
    }

    @Test
    public void findsModeFromCharactersInString() {

        assertThat(Code.mode(null), is(nullValue()));
        assertThat(Code.mode(""), is (nullValue()));
        assertThat(Code.mode("abcb"), is ('b'));
        assertThat(Code.mode("cbbc"), is ('c'));
        // other test cases for mode() method
    }

    @Test
    public void removesDuplicates() {
        assertThat(Code.removeDuplicates(arrayOf( 100, 0, 3, 4, 562)), is(arrayOf(100, 0, 3, 4, 562)));

        assertThat(Code.removeDuplicates(arrayOf(0, 100, 0, 3, 4, 562)), is(arrayOf(0, 100, 3, 4, 562)));

        assertThat(Code.removeDuplicates(arrayOf(0, 0, 20, 5)), is(arrayOf(0, 20, 5)));

        assertThat(Code.removeDuplicates(arrayOf(1, 2, 1, 3, 2)), is(arrayOf(1, 2, 3)));

        assertThat(Code.removeDuplicates(arrayOf(1, 1)), is(arrayOf(1)));

        assertThat(Code.removeDuplicates(arrayOf(1, 2, 3)), is(arrayOf(1, 2, 3)));
    }

    @Test
    public void sumsIgnoringDuplicates() {
        assertThat(Code.sumIgnoringDuplicates(arrayOf(1, 1)), is(1));

        assertThat(Code.sumIgnoringDuplicates(arrayOf(1, 2, 1, 3, 2)), is(6));

        assertThat(Code.sumIgnoringDuplicates(arrayOf(1, 2, 3)), is(6));
    }

    private int[] arrayOf(int... numbers) {
        return numbers;
    }

}
